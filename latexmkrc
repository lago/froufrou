@default_files = ('froufrou.dtx');

$custom_latex_opts = ' -synctex=1 -halt-on-error -file-line-error -interaction batchmode ';

$pdf_mode = 4;
$postscript_mode = $dvi_mode = 0;

$silent = 1;
$silence_logfile_warnings = 1;

# Make latexmk -c/-C clean *all* generated files
$cleanup_includes_generated = 1;
$cleanup_includes_cusdep_generated = 1;
$bibtex_use = 2;

add_cus_dep('ins', 'sty', 0, 'genPkg');
sub genPkg{
  return system("pdflatex froufrou.ins");
}

add_cus_dep('glo', 'gls', 0, 'genHistory');
sub genHistory{
  return system("makeindex -s gglo.ist -o froufrou.gls froufrou.glo");
}

push @generated_exts, 'glo', 'gls', 'sty';
